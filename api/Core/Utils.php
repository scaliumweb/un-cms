<?php

class Utils {
    
    public function decryptDataOfMobile($S_text,$S_key,$S_protocole){
        $key = $S_key;
        $iv_sansEncode="1234567890";
        $iv  = base64_decode($iv_sansEncode);
        $cipher = base64_decode($S_text);
        $plain = openssl_decrypt(
           $cipher,
           $S_protocole,
           $key,
           OPENSSL_RAW_DATA | OPENSSL_NO_PADDING,
           $iv    
        );

        return trim($plain);
        //try to detect null padding
        /*if (mb_strlen($iv, '8bit') % mb_strlen($plain, '8bit') == 0) {
           preg_match_all('#([\0]+)$#', $plain, $matches);
           if (mb_strlen($matches[1][0], '8bit') > 1) {
               $plain = rtrim($plain, "\0");
               trigger_error('Detected and stripped null padding. Please double-check results!');
           }
        }*/
    }
}
