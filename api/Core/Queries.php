<?php

include_once 'Database.php';

class Queries extends Database
{
    protected $table;

    public function __construct()
    {
        $this->connect("localhost", "upscale_network", "root", "");
    }

    public function getAllAclByCategory() {

        $q = $this->bdd->prepare("SELECT * FROM acl ORDER BY category"
        );

        $q->execute();
        $results = $q->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function deleteAclByIdRole($idrole) {
            $q = $this->bdd->prepare("DELETE FROM acl_role WHERE id_role = :idrole");
            $B_exec = $q->execute(array('idrole' => $idrole));
            if($B_exec != false){
                return true;
            }else{
                return false;
            }
        }
    
}
