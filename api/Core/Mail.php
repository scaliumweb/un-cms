<?php

class Mail
{

    function sendMail($to, $subject, $message)
    {
        $headers = "MIME-Version: 1.0" . "\n";
        $headers .= "Content-type: text/html; charset=utf-8" . "\r\n";
        $headers .= 'From: no-reply@upscale-network.net>' . "\r\n";

        return mail($to, $subject, $message, $headers);
    }
}