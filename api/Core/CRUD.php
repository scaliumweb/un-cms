<?php

include_once 'Database.php';

class CRUD extends Database
{
    protected $table;

    public function __construct()
    {
        $this->connect("localhost", "upscale_network", "root", "");
    }

    public function getPrimaryKey()
    {

        if ($this->table) {
            if (is_string($this->table)) {
                $q = $this->bdd->prepare(
                    "select c.COLUMN_NAME as id_column
                from INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk , INFORMATION_SCHEMA.KEY_COLUMN_USAGE c 
                where pk.TABLE_NAME = '" . $this->table . "'
                and CONSTRAINT_TYPE = 'PRIMARY KEY' 
                and c.TABLE_NAME = pk.TABLE_NAME 
                and c.TABLE_SCHEMA = 'upscale_network' 
                and c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME"
                );
            } else return false;

        } else return false;

        $exec = $q->execute();

        if ($exec) {
            $result = $q->fetch(PDO::FETCH_ASSOC);
            if ($result) {
                if (is_array($result)) {
                    if ($result['id_column']) {
                        if ($result['id_column'] != null || $result['id_column'] != "") {
                            if (is_string($result['id_column'])) {
                                return $result['id_column'];
                            } else return false;
                        } else return false;
                    } else return false;
                } else return false;
            } else return false;
        } else return false;
    }

    public function read()
    {
        if ($this->table) {
            if (is_string($this->table)) {
                $q = $this->bdd->prepare("SELECT * FROM " . $this->table);
            } else return false;
        } else return false;

        $exec = $q->execute();

        if ($exec) {
            $results = $q->fetchAll(PDO::FETCH_ASSOC);
            if ($results) {
                if (is_array($results)) {
                    return $results;
                } else return false;
            } else return false;
        } else return false;


    }

    public function readById($id)
    {
        if ($this->table) {
            if (is_string($this->table)) {
                $id_column = $this->getPrimaryKey();
                if ($id_column) {
                    if (is_string($id_column)) {
                        if ($id) {
                            if (is_string($id)) {

                                $q = $this->bdd->prepare("SELECT * FROM " . $this->table . " WHERE " . $id_column . " = :id ");
                                $donnees = array('id' => $id);
                            } else return false;
                        } else return false;
                    } else return false;
                } else return false;
            } else return false;
        } else return false;

        $exec = $q->execute($donnees);

        if ($exec) {
            $results = $q->fetchAll(PDO::FETCH_ASSOC);
            if ($results) {
                if (is_array($results)) {
                    return $results;
                } else return false;
            } else return false;
        } else return false;

    }

    public function readByField($array)
    {
        if ($this->table) {
            if (is_string($this->table)) {
                if ($array) {
                    if (is_array($array)) {
                        $data = "";
                        $données = array();
                        foreach ($array as $key => $a) {
                            $données[$key] = $a;
                            if (is_string($a)) {
                                $a = "'" . $a . "'";
                            }
                            if (is_string($key)) {
                                $data .= $key . " = :$key,";
                            } else return false;
                        }
                    } else return false;
                } else return false;
            } else return false;
        } else return false;

        if ($data) {
            if (is_string($data)) {
                if ($data != "") {
                    $data = substr($data, 0, -1);
                    $q = $this->bdd->prepare("SELECT * FROM " . $this->table . " WHERE " . $data);
                } else return false;
            } else return false;
        } else return false;

        $exec = $q->execute($données);

        if ($exec) {
            $results = $q->fetchAll(PDO::FETCH_ASSOC);
            if ($results) {
                if (is_array($results)) {
                    return $results;
                } else return false;
            } else return false;
        } else return false;
    }


    public function readByFields($array)
    {
        if ($this->table) {
            if (is_string($this->table)) {
                if ($array) {
                    if (is_array($array)) {
                        $data = "";
                        $données = array();
                        foreach ($array as $key => $a) {
                            $données[$key] = $a;
                            if (is_string($a)) {
                                $a = "'" . $a . "'";
                            }
                            if (is_string($key)) {
                                $data .= $key . " = :$key AND ";
                            } else return false;
                        }
                    } else return false;
                } else return false;
            } else return false;
        } else return false;

        if ($data) {
            if (is_string($data)) {
                if ($data != "") {
                    $data = substr($data, 0, -5);
                    $q = $this->bdd->prepare("SELECT * FROM " . $this->table . " WHERE " . $data);
                } else return false;
            } else return false;
        } else return false;

        $exec = $q->execute($données);

        if ($exec) {
            $results = $q->fetchAll(PDO::FETCH_ASSOC);
            if ($results) {
                if (is_array($results)) {
                    return $results;
                } else return false;
            } else return false;
        } else return false;
    }

    public function create($array)
    {
        if ($this->table) {
            if (is_string($this->table)) {
                if ($array) {
                    if (is_array($array)) {
                        $data = "";
                        $keys = "";
                        $données = array();
                        foreach ($array as $key => $a) {
                            $données[$key] = $a;
                            $data .= ":$key,";
                            if (is_string($key)) {
                                $keys .= $key . ",";
                            } else return false;
                        }
                    }
                }
            }
        }
        if ($data) {
            if (is_string($data)) {
                if ($data != "") {
                    $data = substr($data, 0, -1);
                    if ($keys) {
                        if (is_string($keys)) {
                            if ($keys != "") {
                                $keys = substr($keys, 0, -1);
                                $q = $this->bdd->prepare('INSERT INTO ' . $this->table . '(' . $keys . ') VALUES (' . $data . ')');
                                $exec = $q->execute($données);
                                if ($exec) {
                                    return $exec;
                                } else return false;
                            } else return false;
                        } else return false;
                    } else return false;
                } else return false;
            } else return false;
        } else return false;
    }


    public function update($id, $array)
    {

        if ($this->table) {
            if (is_string($this->table)) {
                $id_column = $this->getPrimaryKey();
                if ($id_column) {
                    if (is_string($id_column)) {
                        if ($array) {
                            if (is_array($array)) {
                                $data = "";
                                $données = array();
                                foreach ($array as $key => $a) {
                                    $données[$key] = $a;
                                    if (is_string($a)) {
                                        $a = "'" . $a . "'";
                                    }
                                    if (is_string($key)) {
                                        $data .= $key . " = :$key,";
                                    } else return false;
                                }
                            } else return false;
                        } else return false;
                    } else return false;
                } else return false;
            } else return false;
        } else return false;

        if ($data) {
            if (is_string($data)) {
                if ($data != "") {
                    $data = substr($data, 0, -1);
                    $q = $this->bdd->prepare('UPDATE ' . $this->table . ' SET ' . $data . ' WHERE ' . $id_column . ' = :id');
                    $données['id'] = $id;
                    $exec = $q->execute($données);
                    if ($exec) {
                        return $exec;
                    } else return false;
                } else return false;
            } else return false;
        } else return false;
    }


    public function delete($id)
    {
        if ($this->table) {
            if (is_string($this->table)) {
                $id_column = $this->getPrimaryKey();
                if ($id_column) {
                    if (is_string($id_column)) {
                        if ($id) {
                            if (is_string($id)) {
                                $q = $this->bdd->prepare("DELETE FROM " . $this->table . " WHERE " . $id_column . " = :id");
                                $donnees = array('id' => $id);
                                $exec = $q->execute($donnees);
                                if ($exec) {
                                    return $exec;
                                } else return false;
                            } else return false;
                        } else return false;
                    } else return false;
                } else return false;
            } else return false;
        } else return false;
    }
}
