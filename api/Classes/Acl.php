<?php

include_once __DIR__ . '/../Core/CRUD.php';

class Acl extends CRUD
{
    protected $table = "acl";

    private $id_acl;

    private $name;

    private $category;

    /**
     * @return mixed
     */
    public function getIdAcl()
    {
        return $this->id_acl;
    }

    /**
     * @param mixed $id_acl
     */
    public function setIdAcl($id_acl)
    {
        $this->id_acl = $id_acl;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }


}