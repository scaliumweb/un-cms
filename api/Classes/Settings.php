<?php

include_once __DIR__.'/../Core/CRUD.php';

class Settings extends CRUD
{
    protected $table = "settings";

    private $id_settings;

    private $site_title;

    private $id_language;

    private $google_api_key;

    private $email_sender;

    private $name_sender;

    private $contact_name;

    private $contact_address;

    private $contact_phone;

    private $contact_mobile;

    private $contact_fax;

    private $contact_email;

    /**
     * @return mixed
     */
    public function getIdSettings()
    {
        return $this->id_settings;
    }

    /**
     * @param mixed $id_settings
     */
    public function setIdSettings($id_settings)
    {
        $this->id_settings = $id_settings;
    }

    /**
     * @return mixed
     */
    public function getSiteTitle()
    {
        return $this->site_title;
    }

    /**
     * @param mixed $site_title
     */
    public function setSiteTitle($site_title)
    {
        $this->site_title = $site_title;
    }

    /**
     * @return mixed
     */
    public function getIdLanguage()
    {
        return $this->id_language;
    }

    /**
     * @param mixed $id_language
     */
    public function setIdLanguage($id_language)
    {
        $this->id_language = $id_language;
    }

    /**
     * @return mixed
     */
    public function getGoogleApiKey()
    {
        return $this->google_api_key;
    }

    /**
     * @param mixed $google_api_key
     */
    public function setGoogleApiKey($google_api_key)
    {
        $this->google_api_key = $google_api_key;
    }

    /**
     * @return mixed
     */
    public function getEmailSender()
    {
        return $this->email_sender;
    }

    /**
     * @param mixed $email_sender
     */
    public function setEmailSender($email_sender)
    {
        $this->email_sender = $email_sender;
    }

    /**
     * @return mixed
     */
    public function getNameSender()
    {
        return $this->name_sender;
    }

    /**
     * @param mixed $name_sender
     */
    public function setNameSender($name_sender)
    {
        $this->name_sender = $name_sender;
    }

    /**
     * @return mixed
     */
    public function getContactName()
    {
        return $this->contact_name;
    }

    /**
     * @param mixed $contact_name
     */
    public function setContactName($contact_name)
    {
        $this->contact_name = $contact_name;
    }

    /**
     * @return mixed
     */
    public function getContactAddress()
    {
        return $this->contact_address;
    }

    /**
     * @param mixed $contact_address
     */
    public function setContactAddress($contact_address)
    {
        $this->contact_address = $contact_address;
    }

    /**
     * @return mixed
     */
    public function getContactPhone()
    {
        return $this->contact_phone;
    }

    /**
     * @param mixed $contact_phone
     */
    public function setContactPhone($contact_phone)
    {
        $this->contact_phone = $contact_phone;
    }

    /**
     * @return mixed
     */
    public function getContactMobile()
    {
        return $this->contact_mobile;
    }

    /**
     * @param mixed $contact_mobile
     */
    public function setContactMobile($contact_mobile)
    {
        $this->contact_mobile = $contact_mobile;
    }

    /**
     * @return mixed
     */
    public function getContactFax()
    {
        return $this->contact_fax;
    }

    /**
     * @param mixed $contact_fax
     */
    public function setContactFax($contact_fax)
    {
        $this->contact_fax = $contact_fax;
    }

    /**
     * @return mixed
     */
    public function getContactEmail()
    {
        return $this->contact_email;
    }

    /**
     * @param mixed $contact_email
     */
    public function setContactEmail($contact_email)
    {
        $this->contact_email = $contact_email;
    }

}