<?php

include_once __DIR__.'/../Core/CRUD.php';

class AclRole extends CRUD
{
    protected $table = "acl_role";

    private $id_acl_role;

    private $id_acl;

    private $id_role;

    /**
     * @return mixed
     */
    public function getIdAclRole()
    {
        return $this->id_acl_role;
    }

    /**
     * @param mixed $id_acl_role
     */
    public function setIdAclRole($id_acl_role)
    {
        $this->id_acl_role = $id_acl_role;
    }

    /**
     * @return mixed
     */
    public function getIdAcl()
    {
        return $this->id_acl;
    }

    /**
     * @param mixed $id_acl
     */
    public function setIdAcl($id_acl)
    {
        $this->id_acl = $id_acl;
    }

    /**
     * @return mixed
     */
    public function getIdRole()
    {
        return $this->id_role;
    }

    /**
     * @param mixed $id_role
     */
    public function setIdRole($id_role)
    {
        $this->id_role = $id_role;
    }



}