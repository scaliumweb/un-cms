<?php

include_once __DIR__ . '/../Core/CRUD.php';

class Page extends CRUD
{
    protected $table = "page";

    private $id_page;
    private $title;
    private $content;
    private $date_add;
    private $date_update;
    private $is_active;
    private $url_simple;

    /**
     * @return mixed
     */
    public function getIdPage()
    {
        return $this->id_page;
    }

    /**
     * @param mixed $id_page
     */
    public function setIdPage($id_page)
    {
        $this->id_page = $id_page;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getDateAdd()
    {
        return $this->date_add;
    }

    /**
     * @param mixed $date_add
     */
    public function setDateAdd($date_add)
    {
        $this->date_add = $date_add;
    }

    /**
     * @return mixed
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * @param mixed $date_update
     */
    public function setDateUpdate($date_update)
    {
        $this->date_update = $date_update;
    }

    /**
     * @return mixed
     */
    public function getisActive()
    {
        return $this->is_active;
    }

    /**
     * @param mixed $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return mixed
     */
    public function getUrlSimple()
    {
        return $this->url_simple;
    }

    /**
     * @param mixed $url_simple
     */
    public function setUrlSimple($url_simple)
    {
        $this->url_simple = $url_simple;
    }


}