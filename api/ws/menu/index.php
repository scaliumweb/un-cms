<?php
include_once __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Menu.php';

$Menu = new Menu();

$datas = $_POST;
if (key_exists("id_menu", $datas)) {
    $menu = $Menu->readById($datas['id_menu']);

    if ($menu) {
        $array = array(
            "result" => "ok",
            "data" => $menu
        );
    } else {
        http_response_code(409);
        die("Cette menu n'existe pas");
    }

} else {
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}


http_response_code(200);
echo json_encode($array);