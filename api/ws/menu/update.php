<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Menu.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/menu');
$logger->log('', 'logs_menu_update', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_menu_update', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_menu_update', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_menu_update', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_menu_update', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_menu_update', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_menu_update', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("id_menu", $datas) && key_exists("json", $datas)) {

        $idMenu = $datas['id_menu'];

        unset($datas['id_menu']);

        $Menu = new Menu();
        $update = $Menu->update($idMenu, $datas);

        if ($update) {

            $menu = $Menu->readById($idMenu);

            if ($menu) {
                $array = array(
                    "result" => "ok",
                    "data" => $menu
                );

                http_response_code(200);
                echo json_encode($array);

            } else {
                $logger->log('', 'logs_menu_update', "Retour : Erreur get menu", Logger::GRAN_VOID);
                http_response_code(503);
                die("Problème lors de la recupération du menu");
            }


        } else {
            $logger->log('', 'logs_menu_update', "Retour : Erreur update", Logger::GRAN_VOID);
            http_response_code(503);
            die("Problème lors de la modification du menu");
        }


    } else {
        $logger->log('', 'logs_menu_update', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_menu_update', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}