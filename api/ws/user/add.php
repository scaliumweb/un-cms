<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/User.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/user');
$logger->log('', 'logs_user_add', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_user_add', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_add', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_user_add', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_add', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_user_add', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_add', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("firstname", $datas) && key_exists("lastname", $datas) && key_exists("gender", $datas) && key_exists("email", $datas) && key_exists("password", $datas) && key_exists("id_role", $datas)) {

        foreach ($datas as $key => &$data) {
            if ($key == "gender") {
                $enum = array("Mr", "Mme", "Mlle");

                if (strlen($data) > 4 || !in_array($data, $enum)) {
                    $logger->log('', 'logs_user_add', 'Erreur dans les données - gender', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Veuillez sélectionner une civilité');
                }
            }
            if ($key == "firstname" || $key == "lastname") {
                if (strlen($data) > 32) {
                    $logger->log('', 'logs_user_add', 'Erreur dans les données - firstname / lastname', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ vide ou trop long');
                }
            }
            if ($key == "email") {
                if (strlen($data) > 128 || (!filter_var($data, FILTER_VALIDATE_EMAIL))) {
                    $logger->log('', 'logs_user_add', 'Erreur dans les données - email', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Veuillez renseigner un email valide');
                }
            }
            if ($key == "password") {
                if (strlen($data) > 32 || strlen($data) < 8) {
                    $logger->log('', 'logs_user_add', 'Erreur dans les données - password', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Veuillez renseigner un mot de passe entre 8 et 32 caractères');
                }
            }
        }

        $User = new User();

        $exist = $User->readByField(array('email' => $datas['email']));

        if (!$exist) {
            $user = array(
                'firstname' => $datas['firstname'],
                'lastname' => $datas['lastname'],
                'gender' => $datas['gender'],
                'email' => $datas['email'],
                'password' => md5($datas['password']),
                'is_active' => 1,
                'is_deleted' => 0,
                'date_add' => date('Y-m-d H:i:s'),
                'date_update' => date('Y-m-d H:i:s'),
                'last_login' => null,
                'id_role' => $datas['id_role']
            );
            $create = $User->create($user);

            if ($create) {
                http_response_code(200);
                echo json_encode(array(
                    'result' => 'ok',
                    'data' => $user
                ));
            } else {
                $logger->log('', 'logs_user_add', 'Erreur  - Erreur a l insertion du user', Logger::GRAN_VOID);
                http_response_code(503);
                die("Problème lors de la creation du user");
            }
        } else {
            $logger->log('', 'logs_user_add', 'Conflit - email deja existant', Logger::GRAN_VOID);
            http_response_code(409);
            die("Cet email est déjà utilisé");
        }
    } else {
        $logger->log('', 'logs_user_add', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_user_add', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}