<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/User.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/user');
$logger->log('', 'logs_user', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_user', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_user', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_user', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user', json_encode($_REQUEST), Logger::GRAN_VOID);

$User = new User();
if ($_POST) {
    $datas = $_POST;
    if (key_exists("id_user", $datas)) {
        $user = $User->readById($datas['id_user']);

        if ($user) {
            $array = array(
                "result" => "ok",
                "data" => $user
            );
        } else {
            $logger->log('', 'logs_user', "user introuvable", Logger::GRAN_VOID);
            http_response_code(409);
            die("Cet utilisateur n'existe pas");
        }

    } else {
        $logger->log('', 'logs_user', "", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }


} else {
    $users = $User->read();

    $array = array(
        "result" => "ok",
        "data" => $users
    );
}

http_response_code(200);
echo json_encode($array);