<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/User.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/user');
$logger->log('', 'logs_user_login', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_user_login', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_login', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_user_login', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_login', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_user_login', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_login', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("email", $datas) && key_exists("password", $datas)) {

        foreach ($datas as $key => &$data) {
            if (is_string($data)) {
                if ($key == "email") {
                    if (strlen($data) > 128 || !filter_var($data, FILTER_VALIDATE_EMAIL)) {
                        http_response_code(400);
                        die('Veuillez renseigner un email valide');
                    }
                }
                if ($key == "password") {
                    if (strlen($data) > 32 || strlen($data) < 8) {
                        http_response_code(400);
                        die('Veuillez renseigner un mot de passe de plus de 8 caractères');
                    }
                }
            } else {
                http_response_code(400);
                die('Erreur dans les données');
            }
        }

        $User = new User();

        $isEmail = $User->readByField(array("email" => $datas['email']));
        $return = false;
        if ($isEmail) {
            if ($isEmail[0]['password'] == md5($datas['password'])) {
                $return = true;
            }
        }

        if ($return) {
            $token = md5($isEmail[0]['id_user'].time());
            $updateTokenAndDate = $User->update($isEmail[0]['id_user'], array('token' => $token, 'last_login' => date('Y-m-d H:i:s')));

            if ($updateTokenAndDate) {
                $isEmail[0]['token'] = $token;
                http_response_code(200);
                echo json_encode(array(
                    "result" => "ok",
                    "data" => $isEmail[0]
                ));
            } else {
                $logger->log('', 'logs_user_login', "Retour : Erreur update", Logger::GRAN_VOID);
                http_response_code(503);
                die("Problème lors de la modification du user");
            }

        } else {
            $logger->log('', 'logs_user_login', "Email ou mot de passe incorrecte", Logger::GRAN_VOID);
            http_response_code(401);
            die('L’email ou le mot de passe n’est pas valide');
        }

    } else {
        $logger->log('', 'logs_user_login', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_user_login', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}