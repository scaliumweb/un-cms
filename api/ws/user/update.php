<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/User.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/user');
$logger->log('', 'logs_user_update', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_user_update', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_update', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_user_update', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_update', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_user_update', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_user_update', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("id_user", $datas)) {

        $idUser = $datas['id_user'];

        unset($datas['id_user']);

        $User = new User();
        $update = $User->update($idUser, $datas);

        if ($update) {

            $user = $User->readById($idUser);

            if ($user) {
                $array = array(
                    "result" => "ok",
                    "data" => $user
                );

                http_response_code(200);
                echo json_encode($array);

            } else {
                $logger->log('', 'logs_user_update', "Retour : Erreur get user", Logger::GRAN_VOID);
                http_response_code(503);
                die("Problème lors de la recupération du user");
            }


        } else {
            $logger->log('', 'logs_user_update', "Retour : Erreur update", Logger::GRAN_VOID);
            http_response_code(503);
            die("Problème lors de la modification du user");
        }


    } else {
        $logger->log('', 'logs_user_update', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_user_update', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}