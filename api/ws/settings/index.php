<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Settings.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/settings');
$logger->log('', 'logs_settings', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_settings', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_settings', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_settings', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_settings', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_settings', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_settings', json_encode($_REQUEST), Logger::GRAN_VOID);

$Settings = new Settings();
if ($_POST) {
    $datas = $_POST;
    if (key_exists("id_settings", $datas)) {
        $settings = $Settings->readById($datas['id_settings']);

        if ($settings) {
            $array = array(
                "result" => "ok",
                "data" => $settings
            );
        } else {
            $logger->log('', 'logs_settings', "settings introuvable", Logger::GRAN_VOID);
            http_response_code(409);
            die("Ce settings n'existe pas");
        }

    } else {
        $logger->log('', 'logs_settings', "", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }

} else {
    $settingss = $Settings->read();

    $array = array(
        "result" => "ok",
        "data" => $settingss
    );
}

http_response_code(200);
echo json_encode($array);