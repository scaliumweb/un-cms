<?php
include_once __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Menu.php';
include_once __DIR__ . '/../../Classes/Settings.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/settings');
$logger->log('', 'logs_settings_add', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_settings_add', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_settings_add', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_settings_add', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_settings_add', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_settings_add', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_settings_add', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("site_title", $datas) && key_exists("id_language", $datas) && key_exists("email_sender", $datas) && key_exists("name_sender", $datas) && key_exists("contact_name", $datas) && key_exists("contact_address", $datas) && key_exists("contact_phone", $datas) && key_exists("contact_fax", $datas) && key_exists("contact_email", $datas)) {

        foreach ($datas as $key => &$data) {
            if ($key == "site_title") {
                if (strlen($data) > 50) {
                    $logger->log('', 'logs_settings_add', 'Erreur dans les données - site_title', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ title vide ou trop long');
                }
            }
            if ($key == "email_sender") {
                if (strlen($data) > 128 || (!filter_var($data, FILTER_VALIDATE_EMAIL))) {
                    $logger->log('', 'logs_settings_add', 'Erreur dans les données - email_sender', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Veuillez renseigner un email valide');
                }
            }
            if ($key == "name_sender") {
                if (strlen($data) > 50) {
                    $logger->log('', 'logs_settings_add', 'Erreur dans les données - name_sender', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ name_sender vide ou trop long');
                }
            }
            if ($key == "contact_name") {
                if (strlen($data) > 50) {
                    $logger->log('', 'logs_settings_add', 'Erreur dans les données - contact_name', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ contact_name vide ou trop long');
                }
            }
            if ($key == "contact_address") {
                if (strlen($data) > 255) {
                    $logger->log('', 'logs_settings_add', 'Erreur dans les données - contact_address', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ contact_address vide ou trop long');
                }
            }
            if ($key == "contact_phone") {
                if (strlen($data) > 10) {
                    $logger->log('', 'logs_settings_add', 'Erreur dans les données - contact_address', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ contact_address vide ou trop long');
                }
            }
            if ($key == "contact_fax") {
                if (strlen($data) > 10) {
                    $logger->log('', 'logs_settings_add', 'Erreur dans les données - contact_address', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ contact_address vide ou trop long');
                }
            }
            if ($key == "contact_email") {
                if (strlen($data) > 128 || (!filter_var($data, FILTER_VALIDATE_EMAIL))) {
                    $logger->log('', 'logs_settings_add', 'Erreur dans les données - contact_email', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Veuillez renseigner un email valide');
                }
            }
        }

        $Settings = new Settings();

        $exist = $Settings->readByField(array('site_title' => $datas['site_title']));

        if (!$exist) {
            $Settings = array(
                'site_title' => $datas['site_title'],
                'id_language' => $datas['id_language'],
                'google_api_key' => $datas['google_api_key'],
                'email_sender' => $datas['email_sender'],
                'name_sender' => $datas['name_sender'],
                'contact_name' => $datas['contact_name'],
                'contact_address' => $datas['contact_address'],
                'contact_phone' => $datas['contact_phone'],
                'contact_mobile' => $datas['contact_mobile'],
                'contact_fax' => $datas['contact_fax'],
                'contact_email' => $datas['contact_email']
            );
            $create = $Settings->create($Settings);

            if ($create) {
                http_response_code(200);
                echo json_encode(array(
                    'result' => 'ok',
                    'data' => $Settings
                ));
            } else {
                $logger->log('', 'logs_settings_add', 'Erreur  - Erreur a l insertion du settings', Logger::GRAN_VOID);
                http_response_code(503);
                die("Problème lors de la creation du settings");
            }
        } else {
            $logger->log('', 'logs_settings_add', 'Conflit - settings deja existante', Logger::GRAN_VOID);
            http_response_code(409);
            die("Ce settings existe déjà");
        }
    } else {
        $logger->log('', 'logs_settings_add', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_settings_add', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}