<?php
include_once __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/AclRole.php';
include_once __DIR__ . '/../../Classes/Role.php';
include_once __DIR__ . '/../../Core/Queries.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/acl');
$logger->log('', 'logs_acl_add', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_acl_add', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_acl_add', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_acl_add', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_acl_add', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_acl_add', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_acl_add', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;
    $Queries = new Queries();
    $AclRole = new AclRole();
    $Role = new Role();

    if (key_exists("acls", $datas) && key_exists("name", $datas)) {

        if (key_exists("id_role", $datas)) {
            $delete = $Queries->deleteAclByIdRole($datas['id_role']);

            if ($delete) {

                $acls = $datas['acls'];

                if (!empty($acls)) {
                    foreach ($acls as $a) {
                        $AclRole->create(array(
                            'id_acl' => $a,
                            'id_role' => $datas['id_role']
                        ));
                    }
                }

                http_response_code(200);
                echo json_encode(array(
                    'result' => 'ok'
                ));

            } else {
                $logger->log('', 'logs_acl_add', 'Conflit - problème lors du delete', Logger::GRAN_VOID);
                http_response_code(409);
                die("Problème lors de la suppression");
            }
        } else {

            $createRole = $Role->create(array(
                'name' => $datas['name']
            ));

            if (!$createRole) {
                die('error lors du create role');
            }

            $newRole = $Role->readByField(array('name' => $datas['name']));

            if (!$newRole) {
                die('error lors du get role');
            }

            $idRole = $newRole[0]['id_role'];

            $acls = $datas['acls'];

            if (!empty($acls)) {
                foreach ($acls as $a) {
                    $AclRole->create(array(
                        'id_acl' => $a,
                        'id_role' => $idRole
                    ));
                }
            }
        }
    } else {
        $logger->log('', 'logs_acl_add', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_acl_add', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}
