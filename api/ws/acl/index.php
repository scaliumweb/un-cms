<?php
include_once __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Core/Queries.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/acl');
$logger->log('', 'logs_acl', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_acl', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_acl', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_acl', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_acl', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_acl', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_acl', json_encode($_REQUEST), Logger::GRAN_VOID);

$Queries = new Queries();
$acls = $Queries->getAllAclByCategory();

if ($acls) {
    $data = array();
    foreach ($acls as $acl) {
        $category = $acl['category'];
        $data[$category][] = array(
            'id_acl' => $acl['id_acl'],
            'name' => $acl['name']
        );
    }
}

$array = array(
    "result" => "ok",
    "data" => $data
);

http_response_code(200);
echo json_encode($array);