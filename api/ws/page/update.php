<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Page.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/page');
$logger->log('', 'logs_page_update', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_page_update', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page_update', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_page_update', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page_update', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_page_update', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page_update', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("id_page", $datas)) {

        $idPage = $datas['id_page'];

        unset($datas['id_page']);

        $Page = new Page();
        $update = $Page->update($idPage, $datas);

        if ($update) {

            $page = $Page->readById($idPage);

            if ($page) {
                $array = array(
                    "result" => "ok",
                    "data" => $page
                );

                http_response_code(200);
                echo json_encode($array);

            } else {
                $logger->log('', 'logs_page_update', "Retour : Erreur get page", Logger::GRAN_VOID);
                http_response_code(503);
                die("Problème lors de la recupération du page");
            }


        } else {
            $logger->log('', 'logs_page_update', "Retour : Erreur update", Logger::GRAN_VOID);
            http_response_code(503);
            die("Problème lors de la modification du page");
        }


    } else {
        $logger->log('', 'logs_page_update', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_page_update', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}