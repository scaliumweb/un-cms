<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Page.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/page');
$logger->log('', 'logs_page_add', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_page_add', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page_add', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_page_add', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page_add', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_page_add', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page_add', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("title", $datas) && key_exists("content", $datas) && key_exists("is_active", $datas) && key_exists("url_simple", $datas)) {

        foreach ($datas as $key => &$data) {
            if ($key == "title") {
                if (strlen($data) > 50) {
                    $logger->log('', 'logs_page_add', 'Erreur dans les données - title', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ title vide ou trop long');
                }
            }
        }

        $Page = new Page();

        $exist = $Page->readByField(array('title' => $datas['title']));

        if (!$exist) {
            $page = array(
                'title' => $datas['title'],
                'content' => $datas['content'],
                'date_add' => date('Y-m-d H:i:s'),
                'date_update' => date('Y-m-d H:i:s'),
                'is_active' => $datas['is_active'],
                'url_simple' => $datas['url_simple']
            );
            $create = $Page->create($page);

            if ($create) {
                http_response_code(200);
                echo json_encode(array(
                    'result' => 'ok',
                    'data' => $page
                ));
            } else {
                $logger->log('', 'logs_page_add', 'Erreur  - Erreur a l insertion de la page', Logger::GRAN_VOID);
                http_response_code(503);
                die("Problème lors de la creation de la page");
            }
        } else {
            $logger->log('', 'logs_page_add', 'Conflit - page deja existante', Logger::GRAN_VOID);
            http_response_code(409);
            die("Cette page existe déjà");
        }
    } else {
        $logger->log('', 'logs_page_add', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_page_add', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}