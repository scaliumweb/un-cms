<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Page.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/page');
$logger->log('', 'logs_page', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_page', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_page', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_page', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_page', json_encode($_REQUEST), Logger::GRAN_VOID);

$Page = new Page();
if ($_POST) {
    $datas = $_POST;
    if (key_exists("id_page", $datas)) {
        $page = $Page->readById($datas['id_page']);

        if ($page) {
            $array = array(
                "result" => "ok",
                "data" => $page
            );
        } else {
            $logger->log('', 'logs_page', "page introuvable", Logger::GRAN_VOID);
            http_response_code(409);
            die("Cette page n'existe pas");
        }

    } else {
        $logger->log('', 'logs_page', "", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }

} else {
    $pages = $Page->read();

    $array = array(
        "result" => "ok",
        "data" => $pages
    );
}

http_response_code(200);
echo json_encode($array);