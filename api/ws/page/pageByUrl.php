<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Page.php';

$Page = new Page();
if ($_POST) {
    $datas = $_POST;
    if (key_exists("url_simple", $datas)) {

        $page = $Page->readByField(array('url_simple' => $datas['url_simple']));

        if ($page) {
            $array = array(
                "result" => "ok",
                "data" => $page
            );
            http_response_code(200);
            echo json_encode($array);
        } else {
            http_response_code(409);
            die("Cette page n'existe pas");
        }

    } else {
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    die('Pas de POST');
}