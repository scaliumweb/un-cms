<?php
include_once __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Role.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/role');
$logger->log('', 'logs_role_delete', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_role_delete', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role_delete', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_role_delete', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role_delete', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_role_delete', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role_delete', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("id_role", $datas)) {

        $idRole = $datas['id_role'];

        $Role = new Role();
        $delete = $Role->delete($idRole);

        if ($delete) {
            $array = array(
                "result" => "ok"
            );

            http_response_code(200);
            echo json_encode($array);
        } else {
            $logger->log('', 'logs_role_delete', "Retour : Erreur delete", Logger::GRAN_VOID);
            http_response_code(503);
            die("Problème lors de la suppression du role");
        }
    } else {
        $logger->log('', 'logs_role_delete', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_role_delete', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}