<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Role.php';
include_once __DIR__ . '/../../Classes/RolePage.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/role');
$logger->log('', 'logs_role_add', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_role_add', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role_add', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_role_add', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role_add', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_role_add', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role_add', json_encode($_REQUEST), Logger::GRAN_VOID);

if ($_POST) {

    $datas = $_POST;

    if (key_exists("name", $datas) && key_exists("pages", $datas)) {

        foreach ($datas as $key => &$data) {
            if ($key == "name") {
                if (strlen($data) > 50) {
                    $logger->log('', 'logs_role_add', 'Erreur dans les données - name', Logger::GRAN_VOID);
                    http_response_code(400);
                    die('Champ vide ou trop long');
                }
            }
        }

        $Role = new Role();

        $exist = $Role->readByField(array('name' => $datas['name']));

        if (!$exist) {
            $role = array(
                'name' => $datas['name'],
            );
            $create = $Role->create($role);

            $newrole = $Role->readByField(array('name' => $datas['name']));

            $rolepage = new AclRole();

            foreach (json_decode($datas['pages']) as $page) {
                $rolepage->create(array(
                    'id_role' => $newrole[0]['id_role'],
                    'id_page' => $page
                ));
            }

            if ($create) {
                http_response_code(200);
                echo json_encode(array(
                    'result' => 'ok',
                    'data' => $role
                ));
            } else {
                $logger->log('', 'logs_role_add', 'Erreur  - Erreur a l insertion du role', Logger::GRAN_VOID);
                http_response_code(503);
                die("Problème lors de la creation du role");
            }
        } else {
            $logger->log('', 'logs_role_add', 'Conflit - role deja existant', Logger::GRAN_VOID);
            http_response_code(409);
            die("Ce role est déjà créé");
        }
    } else {
        $logger->log('', 'logs_role_add', "pas les bons param", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }
} else {
    $logger->log('', 'logs_role_add', "pas du post", Logger::GRAN_VOID);
    http_response_code(405);
    die("Un ou plusieurs champs sont vides");
}