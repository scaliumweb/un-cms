<?php
include_once  __DIR__ . '/../header.php';
include_once __DIR__ . '/../../Classes/Role.php';
include_once __DIR__ . '/../../logging/Logger.class.php';

$logger = new Logger(__DIR__ . '/../../logs/role');
$logger->log('', 'logs_role', "Entrée dans le fichier", Logger::GRAN_VOID);
$logger->log('', 'logs_role', "data en GET: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role', json_encode($_GET), Logger::GRAN_VOID);
$logger->log('', 'logs_role', "data en POST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role', json_encode($_POST), Logger::GRAN_VOID);
$logger->log('', 'logs_role', "data en REQUEST: ", Logger::GRAN_VOID);
$logger->log('', 'logs_role', json_encode($_REQUEST), Logger::GRAN_VOID);

$Role = new Role();
if ($_POST) {
    $datas = $_POST;
    if (key_exists("id_role", $datas)) {
        $role = $Role->readById($datas['id_role']);

        if ($role) {
            $array = array(
                "result" => "ok",
                "data" => $role
            );
        } else {
            $logger->log('', 'logs_role', "role introuvable", Logger::GRAN_VOID);
            http_response_code(409);
            die("Ce role n'existe pas");
        }

    } else {
        $logger->log('', 'logs_role', "", Logger::GRAN_VOID);
        http_response_code(405);
        die("Un ou plusieurs champs sont vides");
    }


} else {
    $roles = $Role->read();

    $array = array(
        "result" => "ok",
        "data" => $roles
    );
}

http_response_code(200);
echo json_encode($array);